package net.nurma.workshopday1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText editText1, editText2, editText3;
    Button button;
    RadioGroup radiogroup1;
//    RadioButton radioButtonJumlah, radioButtonKurang;
//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);

        button = (Button) findViewById(R.id.button);

        radiogroup1 = (RadioGroup) findViewById(R.id.radiogroup1);
//        radioButtonJumlah = (RadioButton) findViewById(R.id.radioButtonJumlah);
//        radioButtonKurang = (RadioButton) findViewById(R.id.radioButtonKurang);
//        addOnListenerButton();
    }

    void functionJumlahkan(View view){
//        EditText angka1 = findViewById(R.id.editText1);
        int angka1 = Integer.valueOf(editText1.getText().toString());
        int angka2 = Integer.valueOf(editText2.getText().toString());


        int selectedRadioButton = radiogroup1.getCheckedRadioButtonId();

        //Get the selected radio button
        RadioButton radioButtonObj = (RadioButton) findViewById(selectedRadioButton);

        int total = 0;

        Toast.makeText(this, radioButtonObj.getText(),
            Toast.LENGTH_SHORT).show();

        //Get the text of selectedRadioButton and process
        if(radioButtonObj.getText().equals("Penjumlahan")){
            total = angka1 + angka2;
        }
        else if(radioButtonObj.getText().equals("Pengurangan")) {
            total = angka1 - angka2;
        }

        editText3.setText(String.valueOf(total));
    }
}
